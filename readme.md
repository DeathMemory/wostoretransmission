# WoStore 数据包详解

测试服务器
地址：202.108.0.85
端口：8888

* 已上传部分源代码，以供参考

### Wostore 消息传递
                    
```seq
Client->Server: Get Regist 
Server->Client: real RegistPacket 
Client->RealServer:RegistPacket 
RealServer->Client:RegistPacketReply
Client->Server:RegistPcketReplay
Server->Client:AuthConnPacket
Client->RealServer:AuthConnPacket
RealServer-->Client:AuthConnReply(KeepAlive)
Client-->Server:AuthConnReply(KeepAlive)
Server-->Client:Keepalive
Note right of Client: send KeepAlive \nafter 5 sec
note right of RealServer:KeepAlive Not \nnecessarily return
Client-->RealServer:Keepalive
RealServer->Client:AuthConnReply(Msgset)
Client->Server:AuthConnReply(Msgset)
Server->Client:MessageSetPacketReply
Client->RealServer:MessageSetPacketReply
```

图片
![](http://pic.k7dj.com/pic/839.png)

### RegistPacket

* get Regist

regist token 的有效期默认为1小时，过时需要重新请求 regist。若未过有效期可以不用提交regist，直接用 AuthConnPacket 提交即可

| 参数        | 示例   | 说明 |
| --------   | -----  | -----  | 
| dmtype   | 0  | 0 表示初始请求Regist  | 
| seq   | i++  | 从0 开始的自增量，static 变量存活于整个APP生命周期  | 
| imsi      |  868436021899957  | -----  | 
| imei      |  868436021899957  | -----  | 
| mac      |  12 : 34 : 56 : 78 : aa : bb | -----  | 
| platformVersion      |  4.4.4  | -----  | 
| phoneBRAND      |  google  | -----  | 
| phoneMODEL      |  Nexus 5  | -----  | 
| appid      |  90808134320150305111054034000  | -----  | 
| userId      |  SessionTool.getInstance().getMyUUID(16)  | 调用 SessionTool 类直接生成并保存该 UUID | 

* ret RegistPacket

| 参数      | 示例  | 说明   | 
| -------- | -----  | ----- | 
| action | regist  | ourserver的action 以此可以判断行为类型 |
| url      | push.wostore.cn| 连接woserver的Url，只负责Regist请求  |
| port   | 8064 |  连接woserver的port |

### AuthConnPacket

* get RegistPacketReply

| 参数      | 示例  | 说明   | 
| -------- | -----  | ----- | 
| dmtype | 1  | 于联通服务器发生交互后的包都用1 | 
| seq | i++  | 从0 开始的自增量，static 变量存活于整个APP生命周期 | 
| uuid | 1234567890abcdef  | uuid 用之前生成的，一个用户只有一个uuid | 
| appid | 90808134320150305111054034000  | ----- | 
| appinfos | [{"appkey":"unipay","channelid":"90808134320150305111054034000_00012243", "sdkver":"2","package":"com.og.danjiddz","appvercode":6540, "appvername":"6.5.4.0","unipayver":"3.2.5"}]  | json串，参考该格式修改对应信息   channelid是appid + channelid | 

* ret AuthConnPacket

| 参数      | 示例  | 说明   | 
| -------- | -----  | ----- | 
| action | authconn  | 固定值可用于行为判断 | 
| token | 5785ba5da80f99cd3bdd9562e6d37261  | ----- | 
| host | 27.115.67.211  | ip 地址，接收到新的Host后，之前与WoServer的连接就可以断掉了，用这个地址重新与WoServer建立连接，并继续下面的请求 | 
| port | 18168  | 这个端口是随机返回的，不固定 | 
| period | 290  | keepAlive包间隔时长，单位（秒） | 

### KeepAlive

* get AuthConnPacketReply
AuthConnPacketReply 包联通服务器会返回两次数据包，需要根据情况判断处理。

| 参数      | 示例  | 说明   | 
| -------- | -----  | ----- | 
|dmtype  | 1  | ----- | 
| seq | i++  | ----- | 
| uuid | -----  | 之前生成过的UUID | 

* ret KeepAlive or  MessageSetPacketReply

- [ ] ret KeepAlive
  
| 参数      | 示例  | 说明   | 
| -------- | -----  | ----- | 
| action | keepalive  | ----- | 
|state  | 0  | 0 或 -1 当为0时才表示验证OK，5秒后开始第一个心跳    第二至第N个心跳的间隔时间为之前保存的 period 时间决定的  | 

- [ ] ret MessageSetPacketReply
如果返回的是 msgset 说明是 MessageSetPacketReply 此时是不需要 seq 自加1的，如果seq加1了，需要减1

| 参数      | 示例  | 说明   | 
| -------- | -----  | ----- | 
| action | msgset  | ----- | 

发送完 MessageSetPacketReply 给联通服务器后即可关闭连接。
