package com.wostore.paysecurity.util;

import java.util.Random;
import java.util.UUID;

public class SessionTool {
	private static SessionTool mTool;
	
    public static SessionTool getInstance() {
        if(SessionTool.mTool == null) {
            SessionTool.mTool = new SessionTool();
        }

        return SessionTool.mTool;
    }
    
    public String getMyUUID(int len) {
        Random rnd = new Random();
        String rndUUID = UUID.randomUUID().toString().replace("-", "");
        StringBuilder v3 = new StringBuilder();
        int v0;
        for(v0 = 0; v0 < len; ++v0) {
            v3.append(rndUUID.charAt(rnd.nextInt(rndUUID.length() - 1)));
        }

        return v3.toString();
    }
}
