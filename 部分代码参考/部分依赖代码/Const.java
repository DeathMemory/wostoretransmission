package com.wostore.paysecurity.util;

public class Const {
    public static final int CLEAR_TYPE_DISPLAYED = 1;
    public static final int CLEAR_TYPE_EXPIRED = 2;
    public static final String CODING_STYLE = "UTF-8";
    public static final byte CONN_TYPE_TCP = 1;
    public static final byte CONN_TYPE_UDP = 2;
    public static final String EXT_DATA_CHANNEL_ID = "channelid";
    public static final String EXT_DATA_IP_ADDRESS = "ip";
    public static final String EXT_DATA_PUSH_OWNER = "owner";
    public static final String EXT_DATA_SEND_REASON = "sendreason";
    public static final String EXT_DATA_SEND_TIME = "sendtime";
    public static final String EXT_DATA_SIM_NETWORK_TYPE = "simtype";
    public static final String EXT_DATA_SIM_SERIAL_NUMBER = "serialnumber";
    public static final String EXT_USER_DATA = "ext_user_data";
    public static final String FILE_NAME_SHARESHTORE = "uid_info";
    public static final int KEEPALIVE_DELAY = 480000;
    public static final int KEEPALIVE_INITIALDELAY = 5000;
    public static final String KEY_NAME_SHARESHTORE = "uid";
    public static final int MAX_PACKET_LENGTH = 1048576;
    public static final int MAX_RECONNECTTCP_TIMES = 3;
    public static final int MAX_TIME_OUT = 300000;
    public static final int MODE = 0;
    public static final int MSG_AGENT_SRV_RETRY_TIME = 300000;
    public static final int MSG_MAX_PACKET_SIZE = 65507;
    public static final String MSG_STATSRV_KEY = "msg_statsrv";
    public static final int NETWORK_CLASS_2_G = 2;
    public static final int NETWORK_CLASS_3_G = 3;
    public static final int NETWORK_CLASS_4_G = 4;
    public static final int NETWORK_CLASS_UNKNOWN = 0;
    public static final int NETWORK_CLASS_WIFI = 1;
    public static final byte NETWORK_TYPE_1xRTT = 7;
    public static final byte NETWORK_TYPE_CDMA = 4;
    public static final byte NETWORK_TYPE_EDGE = 2;
    public static final byte NETWORK_TYPE_EHRPD = 14;
    public static final byte NETWORK_TYPE_EVDO_0 = 5;
    public static final byte NETWORK_TYPE_EVDO_A = 6;
    public static final byte NETWORK_TYPE_EVDO_B = 12;
    public static final byte NETWORK_TYPE_GPRS = 1;
    public static final byte NETWORK_TYPE_HSDPA = 8;
    public static final byte NETWORK_TYPE_HSPA = 10;
    public static final byte NETWORK_TYPE_HSPAP = 15;
    public static final byte NETWORK_TYPE_HSUPA = 9;
    public static final byte NETWORK_TYPE_IDEN = 11;
    public static final byte NETWORK_TYPE_LTE = 13;
    public static final byte NETWORK_TYPE_UMTS = 3;
    public static final byte NETWORK_TYPE_UNKNOW = 0;
    public static final byte NETWORK_TYPE_WIFI = 16;
    public static final int NEW_REGIST_DELAY = 60000;
    public static short PACKET_SEQ = 0;
    public static final int PULL_DELAY = 300000;
    public static final byte PUSH_TYPE_ACTIVITY = 2;
    public static final byte PUSH_TYPE_CLICKDOWNLOAD = 4;
    public static final byte PUSH_TYPE_SLIENTDOWNLOAD = 3;
    public static final byte PUSH_TYPE_SMARTDOWNLOAD = 5;
    public static final String RECORD_KEY_APPKEY = "appkey";
    public static final String RECORD_KEY_PUSHID = "pushid";
    public static final byte REGIST_STATE_NOMAL = 0;
    public static final int REMIND_RESEND_WAIT_TIME = 4000;
    public static final String SDK_LOCAL_STORE = "msg_store";
    public static final int SDK_VERSION = 2;
    public static final int STATSRV_RETRY_MAX_TIME = 7200000;
    public static final int STATSRV_RETRY_TIME = 1200000;
    public static final String STORE_EXT_INFO = "ext_info";
    public static final String STORE_KEEP_CONNECTION = "keep_connection";
    public static final String STORE_KEEP_CONNECTION_PERIOD = "connectperiod";
    public static final String STORE_KEEP_DISCONNECTION_PERIOD = "disconnectperiod";
    public static final String STORE_LAST_CONNECT_TIME = "lastconnecttime";
    public static final String STORE_LAST_REG_TIME = "last_reg_time";
    public static final String STORE_MSG_AGENT_HOST = "msg_agent_host";
    public static final String STORE_MSG_AGENT_PORT = "msg_agent_port";
    public static final String STORE_MSG_PROVIDER_HOST = "msg_provider_host";
    public static final String STORE_MSG_PROVIDER_PORT = "msg_provider_port";
    public static final String STORE_NEW_REGISTE_DELAY = "msgagent_new_regist_delay";
    public static final String STORE_SERVICE_REDELIVERY = "service_redelivery";
    public static final String STORE_SERVICE_STOPPED = "service_stopped";
    public static final String STORE_STATSRV_RETRY = "statsrv_retry_time";
    public static final String STORE_TCP_KEEPALIVE = "msgagent_keepalive_delay";
    public static final String STORE_TCP_PULL_TIME = "msgagent_pull_time";
    public static final String STORE_TCP_VALID_TIME = "msgagent_valid_time";
    public static final String STORE_TOKEN = "token";
    public static final int SUBPACKAGE_WAIT_TIME = 2000;
    public static final String TOTAL_APPKEYS = "total_appkeys";
    public static int UDP_LOCAL_PORT = 0;
    public static final int UDP_RETRY_TIME_DEFAULT = -1;
    public static final String UNIPUSHINFO_DESC = "desc";
    public static final String UNIPUSHINFO_DISPLAYTYPE = "displayType";
    public static final String UNIPUSHINFO_DOWNLOADURL = "downloadUrl";
    public static final String UNIPUSHINFO_ICONURL = "iconUrl";
    public static final String UNIPUSHINFO_PACKAGENAME = "package";
    public static final String UNIPUSHINFO_PUSHTYPE = "pushType";
    public static final String UNIPUSHINFO_RESSIZE = "resSize";
    public static final String UNIPUSHINFO_TITLE = "title";
    public static final String UNIPUSHINFO_VERCODE = "verCode";
    public static final int UUID_NUMBER = 16;
    public static final String UU_MSG_KEY = "com.unicom.key";

    static {
        Const.UDP_LOCAL_PORT = 8889;
        Const.PACKET_SEQ = 1;
    }

    public Const() {
        super();
    }

	public static int getSeq() {
		synchronized (Const.class) {
			short s = PACKET_SEQ;
			int n = -1 >>> 1;
			if (s > n) {
				PACKET_SEQ = s = 1;
			}
			s = PACKET_SEQ;
			PACKET_SEQ = (short) (n = (int) ((short) (s + 1)));
			return s;
		}
	}

    public static byte getVersion() {
        return 2;
    }
}

